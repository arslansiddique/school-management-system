<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\Parentuser;

Route::get('/', function () {
    return view('admin.dashboard');
    // return App\User::all();
});

Route::group(['prefix'=>'admin','as'=>'admin.'], function(){
	Route::get('parents/create', ['as'=>'parents.create','uses'=>'AdminParentsController@create']);
	Route::post('parents/create', ['as'=>'parents.store','uses'=>'AdminParentsController@postCreate']);
	Route::get('parents/list',function(){
		return Parentuser::with('user')->get();
	});
});

/*=============================================
=            Authentication Routes            =
=============================================*/

Route::get('login', 'Auth\LoginController@signin');
Route::post('signin', 'Auth\LoginController@postSignin');

/*=====  End of Authentication Routes  ======*/
