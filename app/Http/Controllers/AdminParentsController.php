<?php

namespace App\Http\Controllers;

use App\Models\ParentUser;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class AdminParentsController extends Controller
{
    /**
     * Create Parents
     * @return View
     */
    public function create()
    {
    	return view('admin.parents.create');
    }
    /**
     * post Create Parent
     * @return Response 
     */
    public function postCreate(Request $request)
    {
        // dd($request->all());
        $parent_user = Sentinel::registerAndActivate(array(
            'email'       => $request->get('email'),
            'password'    => $request->get('password'),
            'first_name'  => $request->get('first_name'),
            'last_name'   => $request->get('last_name'),
        ));
        $profile = new ParentUser;
        $profile->address = $request->get('address'); 
        $profile->phone = $request->get('phone');
        $profile->gender = $request->get('gender');
        $profile->profession = $request->get('profession');
        $profile->user_id = $parent_user->id;
        $parent_user->roles()->attach(5);
        $imageData = $request->get('image');
        $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
        Image::make($request->get('image'))->save(public_path('images/').$fileName);
        // $profile->associate($parent_user);
        $profile->image = $fileName;
        $profile->save();
    	return $request->all();
    }
    
}
