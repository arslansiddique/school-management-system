<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function signin()
    {
        return view('admin.login');
    }
    /**
     * Account sign in form processing.
     * @param Request $request
     * @return Redirect
     */
    public function postSignin(Request $request)
    {
    // return $request->all();
    try {
        // Try to log the user in
        // dd($request->only(['email', 'password']));
        if($request->has('loginkeeping'))
        {
            if (Sentinel::authenticateAndRemember($request->only(['email', 'password']), $request->get('loginkeeping', false)))
            {
                // Redirect to the dashboard page
                return Redirect::route("home")->with('success', Sentinel::getUser()->profile->legal_name.' you are Logged in Successfully!');
            }
        }
        else
        {
            if (Sentinel::authenticate($request->only(['email', 'password']), $request->get('loginkeeping', false)))
            {
                // Redirect to the dashboard page
                return redirect("/")->with('success', Sentinel::getUser()->email.' you are Logged in Successfully!');
            }   
        }
        

        $this->messageBag->add('email', Lang::get('auth/message.account_not_found'));

    } catch (NotActivatedException $e) {
        $this->messageBag->add('email', Lang::get('auth/message.account_not_activated'));
    } catch (ThrottlingException $e) {
        $delay = $e->getDelay();
        $this->messageBag->add('email', Lang::get('auth/message.account_suspended', compact('delay' )));
    }
    // Ooops.. something went wrong
    return back()->withInput()->withErrors($this->messageBag);
    }
    
}
