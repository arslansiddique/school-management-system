<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParentUser extends Model
{
    protected $table = 'parents';
    protected $fillable = ['name', 'address', 'user_id', 'profession', 'phone'];

    // function students()
    // {
    // 	return $this->hasMany('App\Models\Student','parent_id','id');
    // }
    /**
     * Realtion with User Model
     * @return $query 
     */
    
    public function user()
	{
		return $this->belongsTo('App\User');
	}
}
