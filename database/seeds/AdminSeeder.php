<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
		DB::table('users')->truncate(); // Using truncate function so all info will be cleared when re-seeding.
		DB::table('roles')->truncate();
		DB::table('role_users')->truncate();
		DB::table('activations')->truncate();

		$admin = Sentinel::registerAndActivate(array(
			'email'       => 'admin@admin.com',
			'password'    => "admin",
			'first_name'  => 'John',
			'last_name'   => 'Doe',
		));
		// Create Admin Role
		$adminRole = Sentinel::getRoleRepository()->createModel()->create([
			'name' => 'Admin',
			'slug' => 'admin',
			'permissions' => array('admin' => 1),
		]);
		// App\Models\Profile::create(['user_id'=>$admin->id,'image'=>'profile-default.png']);
		// Create Agent Role
		Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'Employee',
			'slug'  => 'employee',
		]);
		// Create Lender Role
		Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'Teacher',
			'slug'  => 'teacher',
		]);
		// //  Create Company Role
		Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'Student',
			'slug'  => 'student',
		]);
		// //  Create Company Role
		Sentinel::getRoleRepository()->createModel()->create([
			'name'  => 'Parent',
			'slug'  => 'parent',
		]);
		// //  Create Buyer Role
		// Sentinel::getRoleRepository()->createModel()->create([
		// 	'name'  => 'Buyer',
		// 	'slug'  => 'buyer',
		// ]);
		// //  Create Company User Role
		// Sentinel::getRoleRepository()->createModel()->create([
		// 	'name'  => 'Company User',
		// 	'slug'  => 'company_user',
		// ]);

		$admin->roles()->attach($adminRole);

		$this->command->info('Admin User created with username admin@admin.com and password admin');
		DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
