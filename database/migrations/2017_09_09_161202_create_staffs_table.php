<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('designation')->nullable();
            $table->string('email')->nullable();
            $table->string('ntn_no')->nullable();
            $table->string('phone')->nullable();
            $table->integer('basic_salary')->nullable();
            $table->integer('house_rent')->nullable();
            $table->integer('medical')->nullable();
            $table->integer('mobile_allowance')->nullable();
            $table->integer('car_allowance')->nullable();
            $table->integer('other')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
