<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSclassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feestructures', function(Blueprint $table){
            $table->increments('id');
        //  $table->integer('sclass_id')->unsigned()->nullable();
            $table->string('name');
            $table->integer('tution_fee');
            $table->timestamps();
        //  $table->foreign('sclass_id')->references('id')->on('sclasses');
        });

        Schema::create('sclasses', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('class_numeric');
            $table->integer('feestructure_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('feestructure_id')->references('id')->on('feestructures')->onDelete('cascade');

        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sclasses');
        Schema::dropIfExists('feestructures');
    }
}
