<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('class_id');
            $table->integer('parent_id');
            $table->string('roll_no');
            $table->date('birthday');
            $table->string('address');
            $table->string('gender');
            $table->string('phone');
            $table->string('photo');
            $table->string('birth_city')->nullable();
            $table->string('birth_country')->nullable();
            $table->string('nationality')->nullable();
            $table->string('first_language')->nullable();
            $table->string('language_other')->nullable();
            $table->timestamps();

       //     $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }

}
