<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('discription')->nullable();
            $table->string('author')->nullable();
            $table->integer('status');
            $table->float('price')->nullable();
            $table->integer('sclass_id')->unsigned();
            $table->timestamps();
            // $table->foreign('class_id')->references('id')->on('sclasses')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
