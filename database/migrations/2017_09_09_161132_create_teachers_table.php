<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('designation');
            $table->string('ntn_no');
            $table->date('birthday');
            $table->string('sex');
            $table->string('religion');
            $table->string('blood_group');
            $table->string('address');
            $table->string('phone');
            $table->string('account_no')->nullable();
            $table->string('reference')->nullable();
            $table->string('reference_phone')->nullable();
            $table->timestamps();

        //  $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        //  $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
