<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffsalaryslipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffsalaryslips', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('staff_id')->unsigned();
            $table->boolean('status');
            $table->date('date');
            $table->integer('basic_salary');
            $table->integer('house_rent');
            $table->integer('medical');
            $table->integer('mobile_allowance');
            $table->integer('car_allowance');
            $table->integer('other');
            $table->integer('utilities');
            $table->integer('gross_salary');
            $table->integer('pf_own');
            $table->integer('eobi_own');
            $table->integer('income_tax');
            $table->integer('absent_deduction');
            $table->integer('hc_deduction');
            $table->integer('other_deduction');
            $table->integer('total_deduction');
            $table->integer('net_salary');
            $table->timestamps();
            $table->foreign('staff_id')->references('id')->on('staff')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staffsalaryslips');
    }
}
