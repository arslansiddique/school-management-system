<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('discription');
            $table->integer('tution_fee')->nullable();
            $table->integer('registration_fee')->nullable();
            $table->integer('security_fee')->nullable();
            $table->integer('admission_fee')->nullable();
            $table->integer('other_fee')->nullable();
            $table->integer('amount');
            $table->date('payment_time');
            $table->string('payment_method');
            $table->text('payment_details');
            $table->boolean('status')->default(false);
            $table->integer('student_id')->unsigned();
            $table->timestamps();

            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
        });
//      Custom Invoice fee for Students
        Schema::create('customfees', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('tution_fee')->nullable();
            $table->integer('registration_fee')->nullable();
            $table->integer('security_fee')->nullable();
            $table->integer('admission_fee')->nullable();
            $table->integer('student_id')->unsigned();
            $table->timestamps();
            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
        Schema::dropIfExists('customfees');
    }
}
