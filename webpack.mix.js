let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.js('resources/assets/js/page_parents.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.copy('resources/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js', 'public/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js');
// mix.copy('bower_components/waypoints/lib/jquery.waypoints.min.js', 'public/plugins/waypoints/jquery.waypoints.min.js');
// mix.copy('resources/assets/plugins/counterup/jquery.counterup.min.js', 'public/plugins/counterup/jquery.counterup.min.js');
// mix.copy('resources/assets/plugins/jquery-knob/jquery.knob.js', 'public/plugins/jquery-knob/jquery.knob.js');
// mix.copy('resources/assets/plugins/morris/morris.min.js', 'public/plugins/morris/morris.min.js');
// mix.copy('resources/assets/plugins/raphael/raphael-min.js', 'public/plugins/raphael/raphael-min.js');
// mix.copy('resources/assets/plugins/jquery-sparkline/jquery.sparkline.min.js', 'public/plugins/jquery-sparkline/jquery.sparkline.min.js');
// mix.copy('resources/assets/js/jquery.core.js', 'public/js/jquery.core.js');
// mix.copy('resources/assets/js/jquery.app.js', 'public/js/jquery.app.js');
// mix.copy('resources/assets/js/modernizr.min.js', 'public/js/modernizr.min.js');
// mix.copy('resources/assets/js/metisMenu.min.js', 'public/js/metisMenu.min.js');
// mix.copy('resources/assets/js/waves.js', 'public/js/waves.js');
// mix.copy('resources/assets/js/jquery.slimscroll.js', 'public/js/jquery.slimscroll.js');
// mix.copy('resources/assets/pages/jquery.dashboard-2.js', 'public/js/pages/jquery.dashboard-2.js');