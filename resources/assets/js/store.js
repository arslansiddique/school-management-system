import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
  	parents: [],
  	users: [
		{id: 1,first_name: 'Arslan'},
		{id: 2,first_name: 'Salman'},
		{id: 3,first_name: 'Tariq'},
		{id: 4,first_name: 'Kashif'},
	],
	registrations: []
  },
  actions: {
  	LOAD_PARENTS_LIST: function ({ commit }) {
      axios.get('list').then((response) => {
        commit('SET_PARENTS_LIST', { list: response.data })
      }, (err) => {
        console.log(err)
      })
    }
  },
  mutations: {
  	SET_PARENTS_LIST: (state, { list }) => {
      state.parents = list
    }
  },
  getters: {
  },  
  modules: {
    
  }
});

export default store
