
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import store from './store';
Vue.component('parents', require('./components/Parentslist.vue'));
Vue.component('example', require('./components/Example.vue'));


const app = new Vue({
    el: '#parent_content',
    store: store,
    mounted: function () {
        this.$store.dispatch('LOAD_PARENTS_LIST')
    },
    data: {
    	formData: {'first_name':'','last_name':'','email':'','password':'','address':'','gender':'','phone':'','profession':'','image':{}},
    	url: base_url,
    },
    methods: {
    	addParentSubmit: function(){
    		axios.post(this.url+'/create',this.formData).then((response) => {
    			console.log(response);    		
    		});
    	},
        onFileChange(e) {
            let files = e.target.files || e.dataTransfer.files;
            if (!files.length)
                return;
            this.createImage(files[0]);
        },
        createImage(file) {
            let reader = new FileReader();
            let vm = this;
            reader.onload = (e) => {
                vm.formData.image = e.target.result;
            };
            reader.readAsDataURL(file);
        },
    }
});
