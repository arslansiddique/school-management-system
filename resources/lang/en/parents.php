<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Parents Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'addparents' => 'ADD Parents',
    'parents' => 'Parents',
    'first-name' => 'First Name',
    'last-name' => 'Last Name',
    'email' => "Email",
    'password' => "Password",
    'address' => "Address",
    'gender' => "Gender",
    'phone' => "Phone",
    'profession' => "Profession",
    'add-parent-title' => 'You Can Add New Entry for Parent here.'

];
