<!DOCTYPE html>
<html>
    <head>
		@include('admin.layouts.head')
		@yield('styles')
    </head>
    <body>
        <!-- Begin page -->
        <div id="wrapper">
			@include('admin.layouts.header')
			@include('admin.layouts._left_menu')
			<!-- ============================================================== -->
			<!-- Start right Content here -->
			<!-- ============================================================== -->
			<div class="content-page">

			 @yield('content')

			</div>
            <footer class="footer text-right">
                2017 © Adminox. - Coderthemes.com
            </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Right content here -->
        <!-- ============================================================== -->
        {{-- </div> --}}
		@yield('scripts')
		@include('admin.layouts.footer')
    </body>
</html>

