        

<!-- END wrapper -->
<!-- jQuery  -->

        {{-- <script src="assets/js/jquery.min.js"></script> --}}
{{-- <script src="assets/js/bootstrap.min.js"></script> --}}
<script src="{{asset('js/metisMenu.min.js')}}"></script>
<script src="{{asset('js/waves.js')}}"></script>
<script src="{{asset('js/jquery.slimscroll.js')}}"></script>
<!-- Counter js  -->
<script src="{{asset('plugins/waypoints/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('plugins/counterup/jquery.counterup.min.js')}}"></script>

<!-- KNOB JS -->
<!--[if IE]>
<script type="text/javascript" src="../plugins/jquery-knob/excanvas.js"></script>
<![endif]-->
<script src="{{asset('plugins/jquery-knob/jquery.knob.js')}}"></script>

<!--Morris Chart-->
<script src="{{asset('plugins/morris/morris.min.js')}}"></script>
<script src="{{asset('plugins/raphael/raphael-min.js')}}"></script>

<!-- Sparkline charts -->
<script src="{{asset('plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>

<!-- Dashboard init -->
<script src="{{asset('js/pages/jquery.dashboard-2.js')}}"></script>

<!-- App js -->
<script src="{{asset('js/jquery.core.js')}}"></script>
<script src="{{asset('js/jquery.app.js')}}"></script>
