@extends('admin.layouts.default')

@section('styles')
    <style>
        .radio-mf span {
            /* style this span element so we can display nicely, this stlying is not neccessary */
            margin: 0px 0;
            display: block;
            width: 30%;
            float: left;
        }

        .radio-mf input[type="radio"], 
        .radio-mf input[type="checkbox"] {
            /* hide the inputs */
            opacity: 0;
        }

        /* style your lables/button */
        .radio-mf input[type="radio"] + label, 
        .radio-mf input[type="checkbox"] + label {
            /* keep pointer so that you get the little hand showing when you are on a button */
            cursor: pointer;
            /* the following are the styles */
            padding: 4px 10px;
            border: 1px solid #ccc;
            background: #efefef;
            color: #aaa;
            border-radius: 3px;
            text-shadow: 1px 1px 0 rgba(0,0,0,0);
        }

        .radio-mf input[type="radio"]:checked + label,
        .radio-mf input[type="checkbox"]:checked + label{
             /* style for the checked/selected state */
            background: #777;
            border: 1px solid #444;
            text-shadow: 1px 1px 0 rgba(0,0,0,0.4);
            color: white;
        }
    </style>
@endsection

@section('content')
	<!-- Start content -->
    <div class="content" id="parent_content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Admin - Parent Portal</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">@lang('general.dashboard')</a>
                            </li>
                            <li>
                                <a href="#">@lang('parents.parents')</a>
                            </li>
                            <li class="active">
                                @lang('parents.addparents')
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">@lang('parents.addparents')</h3>
                            <p class="panel-sub-title text-muted">@lang('parents.add-parent-title')</p>
                        </div>
                        <div class="panel-body">
                            <form role="form" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="first-name">@lang('parents.first-name')</label>
                                    <div class="col-sm-9">
                                        <input type="text" v-model="formData.first_name" class="form-control" id="first-name" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="last-name">@lang('parents.last-name')</label>
                                    <div class="col-sm-9">
                                        <input type="text" v-model="formData.last_name" class="form-control" id="last-name" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="exampleInputEmail1">@lang('parents.email')</label>
                                    <div class="col-sm-9">
                                        <input type="email" v-model="formData.email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="exampleInputPassword1">@lang('parents.password')</label>
                                    <div class="col-sm-9">
                                        <input type="password" v-model="formData.password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="gender">@lang('parents.gender')</label>
                                    <div class="col-sm-9 radio-mf">
                                        <span>
                                            <input id="Radio1" v-model="formData.gender" name="Radios" type="radio" value="male" checked="checked">
                                            <label for="Radio1" title="Male"><i class="fa fa-male"></i></label>
                                        </span>
                                        <span>
                                            <input id="Radio2" v-model="formData.gender" name="Radios" type="radio" value="female" >
                                            <label for="Radio2" title="Female"><i class="fa fa-female"></i></label>
                                        </span>
                                        {{-- <input type="text" v-model="formData.gender" class="form-control" id="gender" placeholder="Gender"> --}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="address">@lang('parents.address')</label>
                                    <div class="col-sm-9">
                                        <input type="text" v-model="formData.address" class="form-control" id="address" placeholder="Address">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="phone">@lang('parents.phone')</label>
                                    <div class="col-sm-9">
                                        <input type="text" v-model="formData.phone" class="form-control" id="phone" placeholder="Phone">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="profession">@lang('parents.profession')</label>
                                    <div class="col-sm-9">
                                        <input type="text" v-model="formData.profession" class="form-control" id="profession" placeholder="Profession">
                                    </div>
                                </div>
                                 <img :src="formData.image" class="img-responsive">
                                {{-- <input type="file" v-on:change="onFileChange" class="form-control"> --}}
                                <div class="form-group">
                                    <p>Profile Image Upload</p>
                                    <input type="file" v-on:change="onFileChange" class="filestyle" data-input="false">
                                </div>
                                <button type="submit" class="btn btn-purple waves-effect waves-light" @click.stop.prevent="addParentSubmit()">@lang('general.submit')</button>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- end col -->
                <div class="col-md-8">
                    <div class="panel panel-color panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Parents List</h3>
                            <p class="panel-sub-title text-muted">List of Parents</p>
                        </div>
                        <div class="panel-body">
                            {{-- <parents></parents> --}}
                            <example></example>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- container -->
    </div> <!-- content -->
@endsection

@section('scripts')
    <script>
        var base_url = "{{url('admin/parents')}}";
    </script>
    <script src="{{asset('js/page_parents.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js')}}"></script>
@endsection